<?php

require_once "model.class.php";

class UserModel extends  Model {

    public function validateLogin($username, $password) {
        $stmt = $this->pdo->prepare("SELECT id, password FROM users WHERE username = :username LIMIT 1");
        $stmt->execute(['username' => $username]);
        $user = $stmt->fetch();

        if ($user === false) {
            return false;
        }

        if (password_verify($password, $user['password'])) {
            return $user['id'];
        } else {
            return false;
        }
    }

    public function findUserByID($id) {
        $stmt = $this->pdo->prepare("SELECT id, username, password, email, full_name, id_privilege "
                                . "FROM users WHERE id = :id LIMIT 1");
        $stmt->execute(['id' => $id]);
        return $stmt->fetch();
    }

    public function updatePrivilege($id, $privilege) {
        $stmt = $this->pdo->prepare("UPDATE users SET id_privilege = :privilege WHERE id = :id");
        $stmt->execute(["id" => $id, "privilege" => $privilege]);
    }

    public function validateUsername($username) {
        $errors = "";

        $len = strlen($username);
        $minLen = 4;
        $maxLen = 32;
        if ($len < $minLen || $len > $maxLen) {
            $errors .= "Povolená délka uživatelského jména je " . $minLen . " až " . $maxLen . " znaků.\n";
        }
        if (!ctype_alnum($username)) {
            $errors .= "Uživatelské jméno může obsahovat pouze písmena a čísla.\n";
        }
        if (!$this->isUsernameUnique($username)) {
            $errors .= "Uživatelské jméno je zabrané.\n";
        }

        return $errors;
    }

    public function validatePassword($password) {
        $errors = "";

        if (strlen($password) < 8) {
            $errors .= "Heslo musí obsahovat alespoň 8 znaků.\n";
        }

        return $errors;
    }

    public function isUsernameUnique($username) {
        $stmt = $this->pdo->prepare("SELECT id FROM users "
                                            ." WHERE username = :username LIMIT 1");
        $stmt->execute(['username' => $username]);
        return $stmt->fetch() === false;
    }

    public function getAllUsers() {
        $stmt = $this->pdo->prepare("SELECT * FROM users");
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function addUser($username, $password, $email, $full_name, $id_privilege) {
        $data = [
            "username" => $username,
            "password" => password_hash($password, PASSWORD_BCRYPT, ["cost" => 10]),
            "email" => $email,
            "full_name" => $full_name,
            "id_privilege" => $id_privilege
        ];

        $stmt = $this->pdo->prepare("INSERT INTO users (username, password, email, full_name, id_privilege )"
                                    ."VALUES (:username, :password, :email, :full_name, :id_privilege )");

        $stmt->execute($data);
    }


}


?>