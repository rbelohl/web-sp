<?php

abstract class Model {
    protected $pdo;

    public function __construct() {
        require_once "database-connection.inc.php";
        $this->pdo = getPDO();
    }
}

?>