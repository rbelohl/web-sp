<?php

require_once "controller.class.php";

class IntroController extends Controller {
    public function render() {
        $this->data["title"] = "Úvod";
        $this->data["text"] = "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus et euismod arcu. Praesent fermentum ut eros eu ullamcorper. Donec mauris odio, venenatis vel varius at, imperdiet sed urna. Maecenas sed aliquet nisi, vel convallis quam. Donec efficitur semper dui, eget dictum dolor accumsan a. Nunc sed diam eu enim aliquet malesuada. Proin malesuada non est eget tempus. Maecenas et urna quis libero porttitor fermentum vitae et urna. Praesent ut lectus iaculis, volutpat purus fringilla, tristique leo. Proin venenatis gravida libero ut maximus. In nec leo vel risus accumsan tincidunt lacinia id massa. In eu vehicula enim. Vestibulum pellentesque sit amet leo vel accumsan. Nullam vitae massa viverra, pharetra neque id, vehicula dolor.</p><p>Morbi pretium leo a sem facilisis, et sollicitudin justo vehicula. Fusce risus lacus, dignissim in arcu sit amet, pretium dictum sem. Duis non leo vitae tellus accumsan ultricies. Vestibulum finibus pulvinar tortor, ut bibendum dolor ultricies quis. Phasellus ornare, eros id sollicitudin semper, mauris nunc euismod nunc, id elementum nunc purus quis orci. Nunc dignissim id purus eget dapibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam nec semper metus. Aliquam consectetur diam sed bibendum blandit. Cras eros augue, finibus et metus in, commodo lobortis purus. Vivamus ut dui ut purus rutrum ultricies id a nulla. Nam dui elit, vehicula nec sodales sit amet, luctus eu neque. Maecenas posuere eleifend sodales.</p>";
        echo $this->twig->render("intro.twig", $this->data);
    }
}