<?php
class UserController extends Controller {
    public function __construct($twig) {
        parent::__construct($twig);
        $this->mod = new UserModel();
    }

    public function render() {
        if (isset($_GET["action"]) && $_GET["action"]) {
            if ($_GET["action"] == "update") {
                if (isset($_GET["id"]) && $_GET["id"] && isset($_GET["priv"]) && $_GET["priv"]) {
                    $this->updatePrivilege($_GET["id"], $_GET["priv"]);
                }
            }
        } else {
            $this->viewUsers();
        }

    }

    public function viewUsers() {
        if (!$GLOBALS["login"]->isLoggedIn() || $this->data["id_privilege"] < 3) {
            http_response_code(403);
            die('Forbidden');
        }
        $this->data["users"] = $this->mod->getAllUsers();
        echo $this->twig->render("users.twig", $this->data);
    }

    public function updatePrivilege($id, $privilege) {
        if (!$GLOBALS["login"]->isLoggedIn() || $this->data["id_privilege"] < 3) {
            http_response_code(403);
            die('Forbidden');
        }
        $this->mod->updatePrivilege($id, $privilege);
        self::redirect(USERS_URL);
    }
}
