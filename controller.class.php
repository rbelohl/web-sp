<?php

abstract class Controller {
    protected $twig;
    protected $data;
    protected $mod;

    public function __construct($twig) {
        $this->twig = $twig;
        $this->data = array();

        if ($GLOBALS["login"]->isLoggedIn()) {
            $id = $GLOBALS["login"]->getLoggedInID();
            $userMod = new UserModel();
            $user = $userMod->findUserByID($id);
            $this->data = array_merge($this->data, $user);
        }
    }

    protected static function redirect($url = WEB_URL) {
        header("LOCATION: " . $url);
        die();
    }
}

?>