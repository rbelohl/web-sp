<?php

class RegisterController extends Controller {

   public function render() {
       $mod = new UserModel();

       if ($GLOBALS["login"]->isLoggedIn()) {
           self::redirect();
       }

       $this->data = array();

       if (isset($_POST["registerSubmit"])) {
           $valid = true;
           $username_error = "";
           if (isset($_POST["username"]) && $_POST["username"] != null) {
               $this->data["username"] = $_POST["username"];
               $username_error .= $mod->validateUsername($_POST["username"]);
           } else {
               $username_error .= "Zadejte uživatelké jméno.";
           }
           if ($username_error != "") {
               $this->data["username_error"] = $username_error;
               $valid = false;
           }

           $fullname_error = "";
           if (isset($_POST["fullname"]) && $_POST["fullname"]) {
               $this->data["fullname"] = $_POST["fullname"];
           } else {
               $fullname_error .= "Zadejte jméno a příjmení.";
           }
           if ($fullname_error != "") {
               $this->data["fullname_error"] = $fullname_error;
               $valid = false;
           }

           $password_error = "";
           if (isset($_POST["password"]) && $_POST["password"]) {
               $password_error .= $mod->validatePassword($_POST["password"]);
           } else {
               $password_error .= "Zadejte heslo.";
           }
           if ($password_error != "") {
               $this->data["password_error"] = $password_error;
               $valid = false;
           }

           $email_error = "";
           if (isset($_POST["email"]) && $_POST["email"]) {
               $this->data["email"] = $_POST["email"];
           } else {
               $email_error .= "Zadejte E-mail.";
           }
           if ($email_error != "") {
               $this->data["email_error"] = $email_error;
               $valid = false;
           }

           if ($valid) {
               $mod->addUser($_POST["username"], $_POST["password"], $_POST["email"], $_POST["fullname"], 1);
               self::redirect(LOGIN_URL . "&register=success");
           }
       }

       echo $this->twig->render("register.twig", $this->data);
   }

}

?>