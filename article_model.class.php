<?php

class ArticleModel extends Model {

    public function getAllArticles() {
        $stmt = $this->pdo->prepare("SELECT * FROM articles INNER JOIN users ON users.id = articles.id_author");
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function getAllPublishedArticles() {
        $stmt = $this->pdo->prepare("SELECT "
            ."articles.id, articles.title, articles.abstract, articles.accepted, users.full_name"
            ." FROM articles INNER JOIN users ON users.id = articles.id_author WHERE articles.accepted = 1");
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function getAllWaitingArticles() {
        $stmt = $this->pdo->prepare("SELECT "
            ."articles.id, articles.title, articles.abstract, articles.accepted, users.full_name"
            ." FROM articles INNER JOIN users ON users.id = articles.id_author WHERE articles.accepted = 0");
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function updateAccepted($id, $accepted) {
        $stmt = $this->pdo->prepare("UPDATE articles SET accepted = :accepted WHERE id = :id");
        $stmt->execute(["accepted" => $accepted, "id" => $id]);
    }

    public function getAllArticlesByUser($userID) {
        $stmt = $this->pdo->prepare("SELECT "
            ." articles.id, articles.title, articles.abstract, articles.accepted, users.full_name "
            ." FROM articles INNER JOIN users ON users.id = articles.id_author WHERE articles.id_author = :author");
        $stmt->execute(['author' => $userID]);
        return $stmt->fetchAll();
    }

    public function getArticleByID($id) {
        $stmt = $this->pdo->prepare("SELECT "
            ." articles.id, articles.title, articles.abstract, articles.text, articles.accepted, users.full_name "
            ."FROM articles INNER JOIN users ON users.id = articles.id_author WHERE articles.id = :id LIMIT 1");
        $stmt->execute(['id' => $id]);
        return $stmt->fetchAll()[0];
    }

    public function addArticle($title, $abstract, $text, $userID) {
        $stmt = $this->pdo->prepare("INSERT INTO articles (title, abstract, text, id_author) "
                                    . "VALUES (:title, :abstract, :text, :author)");
        $stmt->execute(["title" => $title, "abstract" => $abstract, "text" => $text, "author" => $userID]);
    }

}

?>