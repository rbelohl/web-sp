-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 09, 2020 at 11:28 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `websp`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_czech_ci NOT NULL,
  `abstract` text COLLATE utf8mb4_czech_ci NOT NULL,
  `text` text COLLATE utf8mb4_czech_ci NOT NULL,
  `id_author` int(11) NOT NULL,
  `accepted` tinyint(2) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `abstract`, `text`, `id_author`, `accepted`) VALUES
(1, 'First Article', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus et euismod arcu. Praesent fermentum ut eros eu ullamcorper. Donec mauris odio, venenatis vel varius at, imperdiet sed urna. Maecenas sed aliquet nisi, vel convallis quam. Donec efficitur semper dui, eget dictum dolor accumsan a. Nunc sed diam eu enim aliquet malesuada. Proin malesuada non est eget tempus. Maecenas et urna quis libero porttitor fermentum vitae et urna. Praesent ut lectus iaculis, volutpat purus fringilla, tristique leo. Proin venenatis gravida libero ut maximus. In nec leo vel risus accumsan tincidunt lacinia id massa. In eu vehicula enim. Vestibulum pellentesque sit amet leo vel accumsan. Nullam vitae massa viverra, pharetra neque id, vehicula dolor.</p><p>Morbi pretium leo a sem facilisis, et sollicitudin justo vehicula. Fusce risus lacus, dignissim in arcu sit amet, pretium dictum sem. Duis non leo vitae tellus accumsan ultricies. Vestibulum finibus pulvinar tortor, ut bibendum dolor ultricies quis. Phasellus ornare, eros id sollicitudin semper, mauris nunc euismod nunc, id elementum nunc purus quis orci. Nunc dignissim id purus eget dapibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam nec semper metus. Aliquam consectetur diam sed bibendum blandit. Cras eros augue, finibus et metus in, commodo lobortis purus. Vivamus ut dui ut purus rutrum ultricies id a nulla. Nam dui elit, vehicula nec sodales sit amet, luctus eu neque. Maecenas posuere eleifend sodales.', '', 2, 1),
(2, 'Second Article', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus et euismod arcu. Praesent fermentum ut eros eu ullamcorper. Donec mauris odio, venenatis vel varius at, imperdiet sed urna. Maecenas sed aliquet nisi, vel convallis quam. Donec efficitur semper dui, eget dictum dolor accumsan a. Nunc sed diam eu enim aliquet malesuada. Proin malesuada non est eget tempus. Maecenas et urna quis libero porttitor fermentum vitae et urna. Praesent ut lectus iaculis, volutpat purus fringilla, tristique leo. Proin venenatis gravida libero ut maximus. In nec leo vel risus accumsan tincidunt lacinia id massa. In eu vehicula enim. Vestibulum pellentesque sit amet leo vel accumsan. Nullam vitae massa viverra, pharetra neque id, vehicula dolor.</p><p>Morbi pretium leo a sem facilisis, et sollicitudin justo vehicula. Fusce risus lacus, dignissim in arcu sit amet, pretium dictum sem. Duis non leo vitae tellus accumsan ultricies. Vestibulum finibus pulvinar tortor, ut bibendum dolor ultricies quis. Phasellus ornare, eros id sollicitudin semper, mauris nunc euismod nunc, id elementum nunc purus quis orci. Nunc dignissim id purus eget dapibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam nec semper metus. Aliquam consectetur diam sed bibendum blandit. Cras eros augue, finibus et metus in, commodo lobortis purus. Vivamus ut dui ut purus rutrum ultricies id a nulla. Nam dui elit, vehicula nec sodales sit amet, luctus eu neque. Maecenas posuere eleifend sodales.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus et euismod arcu. Praesent fermentum ut eros eu ullamcorper. Donec mauris odio, venenatis vel varius at, imperdiet sed urna. Maecenas sed aliquet nisi, vel convallis quam. Donec efficitur semper dui, eget dictum dolor accumsan a. Nunc sed diam eu enim aliquet malesuada. Proin malesuada non est eget tempus. Maecenas et urna quis libero porttitor fermentum vitae et urna. Praesent ut lectus iaculis, volutpat purus fringilla, tristique leo. Proin venenatis gravida libero ut maximus. In nec leo vel risus accumsan tincidunt lacinia id massa. In eu vehicula enim. Vestibulum pellentesque sit amet leo vel accumsan. Nullam vitae massa viverra, pharetra neque id, vehicula dolor.</p><p>Morbi pretium leo a sem facilisis, et sollicitudin justo vehicula. Fusce risus lacus, dignissim in arcu sit amet, pretium dictum sem. Duis non leo vitae tellus accumsan ultricies. Vestibulum finibus pulvinar tortor, ut bibendum dolor ultricies quis. Phasellus ornare, eros id sollicitudin semper, mauris nunc euismod nunc, id elementum nunc purus quis orci. Nunc dignissim id purus eget dapibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam nec semper metus. Aliquam consectetur diam sed bibendum blandit. Cras eros augue, finibus et metus in, commodo lobortis purus. Vivamus ut dui ut purus rutrum ultricies id a nulla. Nam dui elit, vehicula nec sodales sit amet, luctus eu neque. Maecenas posuere eleifend sodales.', 3, 1),
(3, 'Third Article', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus et euismod arcu. Praesent fermentum ut eros eu ullamcorper. Donec mauris odio, venenatis vel varius at, imperdiet sed urna. Maecenas sed aliquet nisi, vel convallis quam. Donec efficitur semper dui, eget dictum dolor accumsan a. Nunc sed diam eu enim aliquet malesuada. Proin malesuada non est eget tempus. Maecenas et urna quis libero porttitor fermentum vitae et urna. Praesent ut lectus iaculis, volutpat purus fringilla, tristique leo. Proin venenatis gravida libero ut maximus. In nec leo vel risus accumsan tincidunt lacinia id massa. In eu vehicula enim. Vestibulum pellentesque sit amet leo vel accumsan. Nullam vitae massa viverra, pharetra neque id, vehicula dolor.</p><p>Morbi pretium leo a sem facilisis, et sollicitudin justo vehicula. Fusce risus lacus, dignissim in arcu sit amet, pretium dictum sem. Duis non leo vitae tellus accumsan ultricies. Vestibulum finibus pulvinar tortor, ut bibendum dolor ultricies quis. Phasellus ornare, eros id sollicitudin semper, mauris nunc euismod nunc, id elementum nunc purus quis orci. Nunc dignissim id purus eget dapibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam nec semper metus. Aliquam consectetur diam sed bibendum blandit. Cras eros augue, finibus et metus in, commodo lobortis purus. Vivamus ut dui ut purus rutrum ultricies id a nulla. Nam dui elit, vehicula nec sodales sit amet, luctus eu neque. Maecenas posuere eleifend sodales.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus et euismod arcu. Praesent fermentum ut eros eu ullamcorper. Donec mauris odio, venenatis vel varius at, imperdiet sed urna. Maecenas sed aliquet nisi, vel convallis quam. Donec efficitur semper dui, eget dictum dolor accumsan a. Nunc sed diam eu enim aliquet malesuada. Proin malesuada non est eget tempus. Maecenas et urna quis libero porttitor fermentum vitae et urna. Praesent ut lectus iaculis, volutpat purus fringilla, tristique leo. Proin venenatis gravida libero ut maximus. In nec leo vel risus accumsan tincidunt lacinia id massa. In eu vehicula enim. Vestibulum pellentesque sit amet leo vel accumsan. Nullam vitae massa viverra, pharetra neque id, vehicula dolor.</p><p>Morbi pretium leo a sem facilisis, et sollicitudin justo vehicula. Fusce risus lacus, dignissim in arcu sit amet, pretium dictum sem. Duis non leo vitae tellus accumsan ultricies. Vestibulum finibus pulvinar tortor, ut bibendum dolor ultricies quis. Phasellus ornare, eros id sollicitudin semper, mauris nunc euismod nunc, id elementum nunc purus quis orci. Nunc dignissim id purus eget dapibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam nec semper metus. Aliquam consectetur diam sed bibendum blandit. Cras eros augue, finibus et metus in, commodo lobortis purus. Vivamus ut dui ut purus rutrum ultricies id a nulla. Nam dui elit, vehicula nec sodales sit amet, luctus eu neque. Maecenas posuere eleifend sodales.', 4, 1),
(4, 'test ', 'test test test test test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', 2, 2),
(5, 'Originální titulek', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?', 5, 0),
(7, 'Další článek', 'According to all known laws of aviation, there is no way a bee should be able to fly. Its wings are too small to get its fat little body off the ground. The bee, of course, flies anyway because bees don\'t care what humans think is impossible. Yellow, black. Yellow, black. Yellow, black. Yellow, black. Ooh, black and yellow! Let\'s shake it up a little.', 'Barry! Breakfast is ready! Ooming! Hang on a second. Hello? - Barry? - Adam? - Oan you believe this is happening? - I can\'t. I\'ll pick you up. Looking sharp. Use the stairs. Your father paid good money for those. Sorry. I\'m excited. Here\'s the graduate. We\'re very proud of you, son. A perfect report card, all B\'s. Very proud. Ma! I got a thing going here. - You got lint on your fuzz. - Ow! That\'s me! - Wave to us! We\'ll be in row 118,000. - Bye! Barry, I told you, stop flying in the house! - Hey, Adam. - Hey, Barry. - Is that fuzz gel? - A little. Special day, graduation. Never thought I\'d make it. Three days grade school, three days high school. Those were awkward. Three days college. I\'m glad I took a day and hitchhiked around the hive. You did come back different. - Hi, Barry. - Artie, growing a mustache? Looks good. - Hear about Frankie? - Yeah. - You going to the funeral? - No, I\'m not going. Everybody knows, sting someone, you die. Don\'t waste it on a squirrel. Such a hothead. I guess he could have just gotten out of the way. I love this incorporating an amusement park into our day. That\'s why we don\'t need vacations. Boy, quite a bit of pomp... under the circumstances. - Well, Adam, today we are men. - We are! - Bee-men. - Amen! Hallelujah! Students, faculty, distinguished bees, please welcome Dean Buzzwell. Welcome, New Hive Oity graduating class of... ...9:15. That concludes our ceremonies. And begins your career at Honex Industries! Will we pick ourjob today? I heard it\'s just orientation. Heads up! Here we go. Keep your hands and antennas inside the tram at all times. - Wonder what it\'ll be like? - A little scary. Welcome to Honex, a division of Honesco and a part of the Hexagon Group. This is it! Wow. Wow. We know that you, as a bee, have worked your whole life to get to the point where you can work for your whole life. Honey begins when our valiant Pollen Jocks bring the nectar to the hive. Our top-secret formula is automatically color-corrected, scent-adjusted and bubble-contoured into this soothing sweet syrup with its distinctive golden glow you know as... Honey! - That girl was hot. - She\'s my cousin! - She is? - Yes, we\'re all cousins. - Right. You\'re right. - At Honex, we constantly strive to improve every aspect of bee existence. These bees are stress-testing a new helmet technology. - What do you think he makes? - Not enough. Here we have our latest advancement, the Krelman. - What does that do? - Oatches that little strand of honey that hangs after you pour it. Saves us millions. Oan anyone work on the Krelman? Of course. Most bee jobs are small ones. But bees know that every small job, if it\'s done well, means a lot. But choose carefully because you\'ll stay in the job you pick for the rest of your life. The same job the rest of your life? I didn\'t know that. What\'s the difference? You\'ll be happy to know that bees, as a species, haven\'t had one day off in 27 million years. So you\'ll just work us to death? We\'ll sure try. Wow! That blew my mind! \"What\'s the difference?\" How can you say that? One job forever? That\'s an insane choice to have to make. I\'m relieved. Now we only have to make one decision in life. But, Adam, how could they never have told us that? Why would you question anything? We\'re bees. We\'re the most perfectly functioning society on Earth. You ever think maybe things work a little too well here? Like what? Give me one example. I don\'t know. But you know what I\'m talking about. Please clear the gate. Royal Nectar Force on approach. Wait a second. Oheck it out. - Hey, those are Pollen Jocks! - Wow. I\'ve never seen them this close. They know what it\'s like outside the hive. Yeah, but some don\'t come back. - Hey, Jocks! - Hi, Jocks! You guys did great! You\'re monsters! You\'re sky freaks! I love it! I love it! - I wonder where they were. - I don\'t know. Their day\'s not planned. Outside the hive, flying who knows where, doing who knows what. You can\'tjust decide to be a Pollen Jock. You have to be bred for that. Right. Look. That\'s more pollen than you and I will see in a lifetime. It\'s just a status symbol. Bees make too much of it. Perhaps. Unless you\'re wearing it and the ladies see you wearing it. Those ladies? Aren\'t they our cousins too? Distant. Distant. Look at these two. - Oouple of Hive Harrys. - Let\'s have fun with them. It must be dangerous being a Pollen Jock.', 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id_reviewer` int(11) NOT NULL,
  `id_article` int(11) NOT NULL,
  `text` text COLLATE utf8mb4_czech_ci NOT NULL,
  `rating_originality` tinyint(4) NOT NULL,
  `rating_language` tinyint(4) NOT NULL,
  `rating_technical` tinyint(4) NOT NULL,
  `rating_overall` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id_reviewer`, `id_article`, `text`, `rating_originality`, `rating_language`, `rating_technical`, `rating_overall`) VALUES
(2, 2, 'Nějaká recenze. ', 7, 8, 9, 8),
(2, 3, 'Text recenze.', 2, 6, 10, 8);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(32) COLLATE utf8mb4_czech_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_czech_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_czech_ci NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_czech_ci NOT NULL,
  `id_privilege` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `full_name`, `id_privilege`) VALUES
(1, 'novak', '$2y$10$vWWmDPxiJHHpPT/B5g91iu7PURpVAGUgwXZ1Xyb00d8YPbnbmp.Zm', 'novak@example.com', 'Jan Novák', 3),
(2, 'rada', '$2y$10$CmNymkkzTP2fzBD1GDGAce4NVaY5DmjkWQ1tk3VEGvn4aO1b9RWJa', 'rbelohl@students.zcu.cz', 'Radek Bělohlávek', 3),
(3, 'aaaaa', '$2y$10$KxA0UlKCPg.XwgVEzv.KeustB.PCXCS3mv/wXGwYYB.jaW4zd9j8m', 'aaaaa@example.com', 'aaaaaa bbbbb', 1),
(4, 'bbbbb', '$2y$10$PbAcC.K5wf9rmD.Zi5bcXed5leMMFV79kmfj8TeL/zlXG0.8WnhNu', 'bbbbb@example.com', 'bbbbb ccccc', 2),
(5, 'franta', '$2y$10$G35kybUJxeRFhM3fhRhonuZpqzt1lu9hc.dNezxLqauoOv7iWU0T6', 'franta@example.com', 'Franta Novotný', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_author` (`id_author`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id_reviewer`,`id_article`),
  ADD KEY `id_reviewer` (`id_reviewer`),
  ADD KEY `id_article` (`id_article`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_ibfk_1` FOREIGN KEY (`id_author`) REFERENCES `users` (`id`);

--
-- Constraints for table `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_ibfk_1` FOREIGN KEY (`id_reviewer`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `reviews_ibfk_2` FOREIGN KEY (`id_article`) REFERENCES `articles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
