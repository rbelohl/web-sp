<?php

class LogoutController extends Controller {

    public function render() {
        $GLOBALS["login"]->logout();
        header("LOCATION: " . WEB_URL);
        die();
    }

}

?>

