<?php

class Login {

    public function __construct() {
        session_start();
    }

    public function isLoggedIn() {
        return isset($_SESSION["user_id"]);
    }

    public function getLoggedInID() {
        if (!$this->isLoggedIn()) {
            return false;
        } else {
            return $_SESSION["user_id"];
        }
    }

    public function login($id) {
        $_SESSION["user_id"] = $id;
    }

    public function logout() {
        unset($_SESSION["user_id"]);
    }
}

?>