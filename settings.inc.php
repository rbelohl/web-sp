<?php

define("WEB_URL", "http://localhost/~rada/web-sp/");
define("INDEX", "index.php?page=");

define("LOGIN_PAGE", "login");
define("INTRO_PAGE", "intro");
define("LOGOUT_PAGE", "logout");
define("REGISTER_PAGE", "register");
define("ARTICLES_PAGE", "articles");
define("REVIEW_PAGE", "review");
define("USERS_PAGE", "users");

define("DEFAULT_PAGE", INTRO_PAGE);

define("CONTROLLERS",
    [   LOGIN_PAGE => "LoginController",
        INTRO_PAGE => "IntroController",
        LOGOUT_PAGE => "LogoutController",
        REGISTER_PAGE => "RegisterController",
        ARTICLES_PAGE => "ArticlesController",
        REVIEW_PAGE => "ReviewController",
        USERS_PAGE => "UserController"
    ]
);

$tmp = WEB_URL . INDEX;

define("LOGIN_URL", $tmp . LOGIN_PAGE);
define("LOGOUT_URL", $tmp . LOGOUT_PAGE);
define("INTRO_URL", $tmp . INTRO_PAGE);
define("REGISTER_URL", $tmp . REGISTER_PAGE);
define("ARTICLES_URL", $tmp . ARTICLES_PAGE);
define("ARTICLES_URL_VIEW_ALL_PUBLISHED", $tmp . ARTICLES_PAGE . "&action=viewAllPublished");
define("ARTICLES_URL_VIEW_ALL_WAITING", $tmp . ARTICLES_PAGE . "&action=viewAllWaiting");
define("ARTICLES_URL_VIEW_MINE", $tmp . ARTICLES_PAGE . "&action=viewMine");
define("ARTICLES_URL_EDIT_NEW", $tmp . ARTICLES_PAGE . "&action=new");
define("ARTICLES_URL_VIEW_ID", $tmp . ARTICLES_PAGE . "&action=view&id=");
define("ARTICLES_URL_ACCEPT", $tmp . ARTICLES_PAGE . "&action=accept&id=");
define("ARTICLES_URL_DENY", $tmp . ARTICLES_PAGE . "&action=deny&id=");
define("REVIEW_URL", $tmp . REVIEW_PAGE . "&id_article=");
define("USERS_URL", $tmp . USERS_PAGE);

?>