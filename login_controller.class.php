<?php

require_once "login.class.php";
require_once "settings.inc.php";
require_once "user_model.class.php";

class LoginController extends  Controller {

    public function __construct($twig) {
        parent::__construct($twig);
    }

    public function render() {
        if ($GLOBALS["login"]->isLoggedIn()) {
            self::redirect();
        }

        if (isset($_GET["register"])) {
            $this->data["register"] = "success";
        }

        if (isset($_POST["username"]) && isset($_POST["password"])) {
            $username = $_POST["username"];
            $password = $_POST["password"];

            $model = new UserModel();
            $valid = $model->validateLogin($username, $password);
            if ($valid) {
                $GLOBALS["login"]->login($valid);
                self::redirect();
            } else {
                $this->data["login_error"] = "Špatné uživatelské jméno nebo heslo.";
                $this->data["username"] = $username;
            }
        }

        echo $this->twig->render("login.twig", $this->data);
    }

}

?>