<?php

class ReviewModel extends Model {
    function getAllReviewsForArticle($articleID) {
        $stmt = $this->pdo->prepare("SELECT * FROM reviews INNER JOIN users ON users.id = reviews.id_reviewer "
            ."WHERE reviews.id_article = :id_article");
        $stmt->execute(["id_article" => $articleID]);
        return $stmt->fetchAll();
    }

    function getReview($userID, $articleID) {
        $stmt = $this->pdo->prepare("SELECT * FROM reviews INNER JOIN users ON users.id = reviews.id_reviewer "
            ."WHERE reviews.id_article = :id_article AND reviews.id_reviewer = :id_user ");
        $stmt->execute(["id_article" => $articleID, "id_user" => $userID]);
        return $stmt->fetchAll();
    }

    function addOrUpdate($userID, $articleID, $text, $originality, $language, $technical, $overall) {
       $stmt = $this->pdo->prepare("INSERT INTO reviews "
           ." (id_reviewer, id_article, text, rating_originality, rating_language, rating_technical, rating_overall) "
           ." VALUES (:user_id, :article_id, :review_text, :originality, :lang, :technical, :overall)"
           ." ON DUPLICATE KEY UPDATE "
           ."   text = VALUES(text), "
           ."   rating_originality = VALUES(rating_originality),"
           ."   rating_language = VALUES(rating_language),"
           ."   rating_technical = VALUES(rating_technical),"
           ."   rating_overall = VALUES(rating_overall)"
       );
       $stmt->execute([ "user_id" => $userID,
                        "article_id" => $articleID,
                        "review_text" => $text,
                        "originality" => $originality,
                        "lang" => $language,
                        "technical" => $technical,
                        "overall" => $overall ]);
    }
}