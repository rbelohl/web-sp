<?php

require_once "review_model.class.php";

class ReviewController extends Controller {
    public function __construct($twig) {
        parent::__construct($twig);
        $this->mod = new ReviewModel();
    }

    public function submit() {
        $articleID = $_GET["id_article"];
        $userID = $GLOBALS["login"]->getLoggedInID();
        $valid = true;

        if (!isset($_POST["originality"]) || !$_POST["originality"]) {
            $valid = false;
        }
        if (!isset($_POST["language"]) || !$_POST["language"]) {
            $valid = false;
        }
        if (!isset($_POST["technical"]) || !$_POST["technical"]) {
            $valid = false;
        }
        if (!isset($_POST["overall"]) || !$_POST["overall"]) {
            $valid = false;
        }
        if (!isset($_POST["text"])) {
            $valid = false;
        }

        if ($valid) {
            $this->mod->addOrUpdate($userID, $articleID, $_POST["text"],
                                    $_POST["originality"], $_POST["language"], $_POST["technical"], $_POST["overall"]);
            $this->data["reviewSuccess"] = true;
        } else {
            echo "ERRR";
        }
    }

    public function render() {

        if ($GLOBALS["login"]->isLoggedIn() && $this->data["id_privilege"] >= 2) {
            if(isset($_GET["id_article"]) && $_GET["id_article"]) {
                if (isset($_POST["reviewSubmit"])) {
                    $this->submit();
                }
                $articleID = $_GET["id_article"];
                $this->data["review"] = $this->mod->getReview($GLOBALS["login"]->getLoggedInID(), $articleID);
                $articleMod = new ArticleModel();
                $this->data["title"] = "Recenze ke článku: " . $articleMod->getArticleByID($articleID)["title"];

                echo $this->twig->render("review.twig", $this->data);
            }
        } else {
            http_response_code(403);
            die('Forbidden');
        }

    }

}
