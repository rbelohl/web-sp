<?php

////////////////////////////////////
// Constants for database connection
////////////////////////////////////
define("DB_SERVER", "localhost");
define("DB_NAME", "websp");
define("DB_USER", "root");
define("DB_PASSWORD", "Kappa123");

function getPDO() {
    $pdo = new PDO("mysql:host=" . DB_SERVER . ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $pdo;
}

?>