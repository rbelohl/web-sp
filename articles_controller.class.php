<?php

require_once "article_model.class.php";
require_once "review_model.class.php";

class ArticlesController extends Controller {

    public function __construct($twig) {
        parent::__construct($twig);
        $this->mod = new ArticleModel();
        $this->reviewMod = new ReviewModel();
    }

    public function render() {
        if (isset($_GET["action"]) && $_GET["action"] != null) {
            $action = $_GET["action"];
            if ($action == "viewAllPublished") {
                $this->viewAllPublished();
            } elseif ($action == "viewAllWaiting") {
                $this->viewAllWaiting();
            } elseif ($action == "viewMine") {
                $userID = $this->data["id"];
                $this->viewArticlesByUser($userID);
            } elseif ($action == "new") {
                $this->newArticle();
            } elseif ($action == "view") {
                if (isset($_GET["id"])) {
                    $this->viewArticle($_GET["id"]);
                }
            } elseif ($action == "accept") {
                if (isset($_GET["id"])) {
                    $this->accept($_GET["id"]);
                }
            } elseif ($action == "deny") {
                if (isset($_GET["id"])) {
                    $this->deny($_GET["id"]);
                }
            }
        } else {
            $this->viewAllPublished();
        }
    }

    public function updateAccepted($id, $accepted) {
        if (!$GLOBALS["login"]->isLoggedIn() || $this->data["id_privilege"] < 3) {
            http_response_code(403);
            die('Forbidden');
        }
        $this->mod->updateAccepted($id, $accepted);
        self::redirect(ARTICLES_URL_VIEW_ID . $id);
    }

    public function accept($id) {
        $this->updateAccepted($id, 1);
    }

    public function deny($id) {
        $this->updateAccepted($id, 2);
    }

    public function viewArticle($id) {
        $this->data["article"] = $this->mod->getArticleByID($id);
        $this->data["reviews"] = $this->reviewMod->getAllReviewsForArticle($id);
        $this->data["title"] = $this->data["article"]["title"];
        echo $this->twig->render("view_article.twig", $this->data);
    }

    public function viewArticlesByUser($userId) {
        $this->data["title"] = "Moje články";
        $this->data["articles"] = $this->mod->getAllArticlesByUser($userId);
        echo $this->twig->render("all_articles.twig", $this->data);
    }

    public function viewAllPublished() {
        $this->data["title"] = "Publikované články";
        $this->data["articles"] = $this->mod->getAllPublishedArticles();
        echo $this->twig->render("all_articles.twig", $this->data);
    }

    public function viewAllWaiting() {
        $this->data["title"] = "Články čekající na schválení";
        $this->data["articles"] = $this->mod->getAllWaitingArticles();
        echo $this->twig->render("all_articles.twig", $this->data);
    }

    public function submit() {
        $valid = true;
        if (isset($_POST["articleTitle"]) && $_POST["articleTitle"]) {
            $this->data["articleTitle"] = $_POST["articleTitle"];
        } else {
            $valid = false;
            $this->data["titleError"] = "Titulek je prázdný.\n";
        }

        if (isset($_POST["articleAbstract"]) && $_POST["articleAbstract"]) {
            $this->data["articleAbstract"] = $_POST["articleAbstract"];
        } else {
            $valid = false;
            $this->data["abstractError"] = "Abstrakt je prázdný.\n";
        }

        if (isset($_POST["articleText"]) && $_POST["articleText"]) {
            $this->data["articleText"] = $_POST["articleText"];
        } else {
            $valid = false;
            $this->data["textError"] = "Text je prázdný.\n";
        }
        if ($valid) {
            $title = $_POST["articleTitle"];
            $abstract = $_POST["articleAbstract"];
            $text = $_POST["articleText"];
            $userID = $GLOBALS["login"]->getLoggedInID();
            $this->mod->addArticle($title, $abstract, $text, $userID);
            self::redirect(ARTICLES_URL_VIEW_MINE);
        }
    }

    public function newArticle() {
        // This page is only for users
        if (!$GLOBALS["login"]->isLoggedIn()) {
            // Just redirect to login page
            self::redirect(LOGIN_URL);
        }

        if (isset($_POST["articleSubmit"])) {
            $this->submit();
        }

        $this->data["title"] = "Nový článek";
        echo $this->twig->render("new_article.twig", $this->data);
    }
}


?>