<?php

require_once 'vendor/autoload.php';

require_once "intro_controller.class.php";
require_once "login_controller.class.php";
require_once "register_controller.class.php";
require_once "logout_controller.class.php";
require_once "articles_controller.class.php";
require_once "review_controller.class.php";
require_once "user_controller.class.php";
require_once "user_model.class.php";
require_once "login.class.php";

$GLOBALS["login"] = new Login();

$loader = new Twig_Loader_Filesystem('templates/');
$twig = new Twig_Environment($loader, array());

$page = DEFAULT_PAGE;
$controllers = CONTROLLERS;

if (isset($_GET["page"]) && $_GET["page"] != null) {
    if (isset($controllers[$_GET["page"]])) {
        $page = $_GET["page"];
    }
}

$controller = new $controllers[$page]($twig);
$controller->render();

?>